#!/usr/bin/env bash
FILE=$1
g++ "$FILE" -std=c++11 -o a.out
echo "Case 1: 
THE DOG JUMPED OVER THE FENCE
Your output:" > output.txt
# shellcheck disable=SC2129
./a.out file1 file2 DOG >> output.txt

echo "Case 2: 
HELLO, TOM GOT A SMALL PIECE OF PIE. SHE ADVISED HIM TO COME BACK AT ONCE.
Your output:" >> output.txt
./a.out Case2a Case2b BACK >> output.txt

echo "Case 3: 
THE DOG JUMPED, OVER+THE
FENCE.!12@ HE
Your output: " >> output.txt
./a.out Case3a Case3b DOG >> output.txt

echo "Case 4:
THE BROWN DOG JUMPED OVER THE LAZY FOX
Your output:" >> output.txt
./a.out Case4a Case4b OVER >> output.txt

echo "Case 5:
THE+BROWN DOG

JUMPED.OVER THE LAZY FOX
Your output:" >> output.txt
./a.out Case5a Case5b OVER >> output.txt

echo "Case 6:
OVER
Your output:" >> output.txt
./a.out Case6a Case6b OVER >> output.txt

echo "Case 7:
OVER OVERF OVERR CVER
Your output:" >> output.txt
./a.out Case7a Case7b OVER >> output.txt

cat output.txt

